package com.gmail.axiom.zbs;

public class QuadraticEquation {
    private double a;
    private double b;
    private double c;
    private String answer;

    private double getDiscriminant() {
        return getB() * getB() - 4.0D * getA() * getC();
    }

    private double getFirstRadical() {
        return (-getB() + Math.sqrt(getDiscriminant())) / (2.0D * getA());
    }

    private double getSecondRadical() {
        return (-getB() - Math.sqrt(getDiscriminant())) / (2.0D * getA());
    }

    private double getRadical() {
        return -(getB() / (2.0D * getA()));
    }


    private double getA() {
        return a;
    }

    private double getB() {
        return b;
    }

    private double getC() {
        return c;
    }

    public QuadraticEquation(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public String getAnswer() {
        if (getDiscriminant() > 0.0D) {
            answer = "Дискриминант больше нуля. Значит, что корня 2. x1 = " + getFirstRadical() + "; x2 = " + getSecondRadical();
        } else if (getDiscriminant() == 0.0D) {
            answer = "Дискриминант равен нулю. Значит, что корень 1. x1 = x2 = " + getRadical();
        } else {
            answer = "Дискриминант меньше нуля. Корней нет.";
        }
        return answer;
    }
}
