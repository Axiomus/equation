package com.gmail.axiom.zbs;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Введите число a");
        double a = Double.parseDouble(reader.readLine());
        System.out.println("Введите число b");
        double b = Double.parseDouble(reader.readLine());
        System.out.println("Введите число c");
        double c = Double.parseDouble(reader.readLine());
        QuadraticEquation quadraticEquation = new QuadraticEquation(a, b, c);
        System.out.println(quadraticEquation.getAnswer());
    }
}
